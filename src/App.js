import logo from './logo.svg'; 
import './App.css';
import {useState} from 'react';
import axios from "axios";





function App() {
  const [city, setCity] = useState('Paris')
  const [temperature, setTemperature] = useState(0);
  const [loading, setLoading] = useState(false)

  function selectCity(cityName, latitude, longitude){
    setLoading(true)
    setCity(cityName)

    axios.get('https://api.open-meteo.com/v1/forecast?latitude='+latitude+'&longitude='+longitude+'&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m')
    .then(data=>{
      const temperatureValue = data.data.current_weather.temperature
      setTemperature(temperatureValue)
      setLoading(false)

    })
    .catch(err=>
      console.log(err))
  }



  return (
    <div className="App">
      <h1>MY WEATHER APP</h1>
      <p>When The Weather is Hot, keep Cool Mind.....</p>
      <p>When the Weather is Cold, keep a Warm Mind......</p>
      <div>
        <button onClick={()=>{selectCity('Kochi',9.93,76.26)}}>Kochi</button>
        <button onClick={()=>{selectCity('Calicut',11.25,75.78)}}>Calicut</button>
        <button onClick={()=>{selectCity('Kottayam',9.59,76.52)}}>Kottayam</button>
      </div>
      {loading?<p>loading....</p>:<p>The current temperature at <span id='city'>{city}</span> is &nbsp;&nbsp;<span id='temperature'>{temperature}°C</span></p>}
    </div>
  );
}

export default App;
